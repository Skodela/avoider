import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Player here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Player extends Actor
{
    /**
     * Act - do whatever the Player wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    //declare some general variables we are going to use.
    int lifetime,cx,cy,px,py;
    MouseInfo pmi;
    MouseInfo mi;
    public void act() 
    {
        // Add your action code here.
        followMouse();
        checkcollide();
    }    
    
    private void followMouse(){
        //makes the Player follow mouse
        //get mouse info
        mi=Greenfoot.getMouseInfo();
        
        //if ok findout where the mouse is and put the player there
        if (mi!=null){
            cx=mi.getX();
            cy=mi.getY();    
            setLocation(cx,cy);
        }
        
        //if we have previous position, then check if player moved much
        //since last round, if they did add some points
        //no points for lazy bums
        if(pmi!=null){
            px=pmi.getX();
            py=pmi.getY();
            if ((px-cx>5) || (cx-px>5)){ lifetime++;}
            if ((py-cy>3) || (cy-py>3)){ lifetime++;}
        }
        
        
        //get the world screen and put score there
        World w=getWorld(); 
        w.showText("Score: "+lifetime,100,300);
        
        //check if player is in top third of screen- if so add 2 more points
        if (cy<w.getHeight()/3){lifetime++; lifetime++;}
        //if player is in middle third then just add one more point
        else if (cy<w.getHeight()/2){lifetime++;}
        
        //save the current mouse pos for next round
        pmi=mi;
    }
    
    private void checkcollide(){    
        //see if there are enough neighbours within 20px of center of the
        //enemy image
        java.util.List lx=getNeighbours(20,false,Blinker.class);
        //lx.size is more than one, means we got hit
        //now we die
        if (lx.size()>0){
          MyWorld mx= (MyWorld) getWorld();
          mx.endGame();
        }
    }
}
