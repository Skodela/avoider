import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Blinker here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Blinker extends Actor
{
    /**
     * Act - do whatever the Blinker wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    int randx, randy, rmode;
    
    public void act() 
    {

        randx=Greenfoot.getRandomNumber(2); //gives 0-4
        randy=Greenfoot.getRandomNumber(2); //gives 0-2
        rmode=Greenfoot.getRandomNumber(2); // gives 0 or 1
        //then add that number to where the enemy is, moving him some amount
        //if rmode is 0 then we move left ; if it is 1 we move right
        //if rmode is 2 then go straight down
        if(rmode==0){ setLocation(getX()-randx,getY()+randy);}
        else if (rmode==1){setLocation(getX()+randx,getY()+randy);}
        else{setLocation(getX(),getY()+randy);}
        checkremove();
        // Add your action code here.
    }    
    
    private void checkremove(){
        World w=getWorld();
        if (getY()>w.getHeight()){w.removeObject(this); }
    }
}
